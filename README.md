# dependencies-comparator

Small tool to compare gradle dependencies output (for now for single --configuration)

## Generating a gradle dependencies file

To generate a dependencies file with gradle use something like: 
eg: 

```gradlew dependencies --configuration=runtimeClasspath --console=plain > filename.txt```

## build this app

`./gradlew assemble`

## run this app

Execute with exactly two generated gradle dependencies files:

```java -jar build/libs/dependencies-comparator.java path/to/filename1.txt path/to/filename2.txt```
