package com.jurrianhartveldt.tools.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

class DependencyTest {

    @ParameterizedTest
    @ValueSource(strings = {
            "+--- javax.websocket:javax.websocket-api -> 1.1",
            "|    |    |    |    +--- com.google.code.findbugs:jsr305:3.0.2",
            "|    |    \\--- com.fasterxml.jackson.core:jackson-annotations:2.12.4"
    })
    void isDependency(final String input) {
        assertThat(Dependency.isDependency(input)).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "> Configure project :",
            "------------------------------------------------------------",
            "Project ':myProject'",
            ""
    })
    void isNotADependency(final String input) {
        assertThat(Dependency.isDependency(input)).isFalse();
    }

    @Test
    void parseRootWithForcedVersion() {
        final String input = "+--- javax.websocket:javax.websocket-api -> 1.1";

        Dependency result = Dependency.parse(input);

        assertSoftly(
                softly -> {
                    softly.assertThat(result.getDepth()).isEqualTo(1);
                    softly.assertThat(result.getGroup()).isEqualTo("javax.websocket");
                    softly.assertThat(result.getArtifact()).isEqualTo("javax.websocket-api");
                    softly.assertThat(result.getVersion()).isEqualTo("1.1");
                    softly.assertThat(result.isVersionOverwritten()).isTrue();
                }
        );
    }

    @Test
    void parseWithDepth11() {
        final String input = "|    |    |    |    |    |    |    |    |         +--- org.apache.httpcomponents:httpclient:4.5.13 (*)";

        Dependency result = Dependency.parse(input);

        assertSoftly(
                softly -> {
                    softly.assertThat(result.getDepth()).isEqualTo(11);
                    softly.assertThat(result.getGroup()).isEqualTo("org.apache.httpcomponents");
                    softly.assertThat(result.getArtifact()).isEqualTo("httpclient");
                    softly.assertThat(result.getVersion()).isEqualTo("4.5.13");
                    softly.assertThat(result.isVersionOverwritten()).isFalse();
                }
        );
    }

    @Test
    void parseEndKeyWithDepth6() {
        final String input = "|    |    |    |    |    \\--- org.eclipse.jetty:jetty-util:9.4.29.v20200521";

        Dependency result = Dependency.parse(input);

        assertSoftly(
                softly -> {
                    softly.assertThat(result.getDepth()).isEqualTo(6);
                    softly.assertThat(result.getGroup()).isEqualTo("org.eclipse.jetty");
                    softly.assertThat(result.getArtifact()).isEqualTo("jetty-util");
                    softly.assertThat(result.getVersion()).isEqualTo("9.4.29.v20200521");
                    softly.assertThat(result.isVersionOverwritten()).isFalse();
                }
        );
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "|    |    |    |    |    \\--- org.eclipse.jetty:jetty-util:9.4.29.v20200521",
            "+--- org.eclipse.jetty:jetty-util -> 9.4.29.v20200521",
            "|    |    |    +--- org.eclipse.jetty:jetty-util -> 9.4.29.v20200521 (*)"
    })
    void getGavKey(String input) {
        assertThat(Dependency.parse(input).getGavKey()).isEqualTo("org.eclipse.jetty:jetty-util:9.4.29.v20200521");
    }
}
