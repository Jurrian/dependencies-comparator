package com.jurrianhartveldt.tools.model;

import org.junit.jupiter.api.Test;

import static com.jurrianhartveldt.tools.model.Dependency.parse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;


class DependencyNodeTest {

    @Test
    void isRoot() {
        DependencyNode rootNode = new DependencyNode(Dependency.rootDependency("My silly root node"));

        assertThat(rootNode.isRoot()).isTrue();
    }

    @Test
    void isNotRoot() {
        Dependency childDependency = parse("+--- com.jurrianhartveldt.tools:dependencies-comparator:1.0.0");
        DependencyNode notRoot = new DependencyNode(Dependency.rootDependency("My silly root node")).createChild(childDependency);

        assertThat(notRoot.isRoot()).isFalse();
    }

    @Test
    void getLevelFromGrandChild() {
        DependencyNode result = new DependencyNode(Dependency.rootDependency("ROOT"))
                .createChild(parse("+--- com.jurrianhartveldt.tools:dependencies-comparator:1.0.0"))
                .createChild(parse("|    +--- org.slf4j:slf4j-api:1.7.32"));

        assertSoftly(
                softly -> {
                    assertThat(result.getLevel()).isEqualTo(2);
                    assertThat(result.getDependency().getDepth()).isEqualTo(2);
                    assertThat(result.getDependency().getArtifact()).isEqualTo("slf4j-api");
                }
        );
    }

    @Test
    void addChild() {
        DependencyNode rootNode = new DependencyNode(Dependency.rootDependency("r00t"));
        DependencyNode dependencyNode = new DependencyNode(parse("+--- com.jurrianhartveldt.tools:dependencies-comparator:1.0.0"));

        rootNode.addChild(dependencyNode);

        assertSoftly(
                softly -> {
                    assertThat(rootNode.getChildren().size()).isEqualTo(1);
                    assertThat(rootNode.getChildren().get(0)).isEqualTo(dependencyNode);
                    assertThat(dependencyNode.getParent()).isEqualTo(rootNode);
                }
        );
    }

    @Test
    void buildRoot() {
        DependencyNode rootNode = new DependencyNode(Dependency.rootDependency("My silly root node"));

        assertThat(rootNode.isRoot()).isTrue();
        assertThat(rootNode.getParent()).isNull();
        assertThat(rootNode.getDependency().getArtifact()).isEqualTo("My silly root node");
    }

    @Test
    void createChild() {
        DependencyNode result = new DependencyNode(Dependency.rootDependency("r00t"))
                .createChild(parse("+--- com.jurrianhartveldt.tools:dependencies-comparator:1.0.0"));

        assertSoftly(
                softly -> {
                    assertThat(result.getParent().getDependency().getArtifact()).isEqualTo("r00t");
                    assertThat(result.getChildren().isEmpty()).isTrue();
                    assertThat(result.getLevel()).isEqualTo(1);
                    assertThat(result.getDependency().getDepth()).isEqualTo(1);
                    assertThat(result.getDependency().getArtifact()).isEqualTo("dependencies-comparator");
                }
        );
    }

    @Test
    void addDependencyResultingInDirectChild() {
        DependencyNode childNode = new DependencyNode(Dependency.rootDependency("rooth"))
                .createChild(parse("+--- com.jurrianhartveldt.tools:dependencies-comparator:1.0.0"));

        Dependency grandChildDependency = parse("|    +--- org.slf4j:slf4j-api:1.7.32");
        DependencyNode result = childNode.add(grandChildDependency);

        assertSoftly(
                softly -> {
                    assertThat(result.getLevel()).isEqualTo(2);
                    assertThat(result.getDependency().getDepth()).isEqualTo(2);
                    assertThat(result.getDependency().getArtifact()).isEqualTo("slf4j-api");
                }
        );
    }

    @Test
    void addSibling() {
        DependencyNode grandChildNode = new DependencyNode(Dependency.rootDependency("root"))
                .createChild(parse("+--- com.jurrianhartveldt.tools:dependencies-comparator:1.0.0"))
                .createChild(parse("|    +--- org.slf4j:slf4j-api:1.7.32"));

        Dependency siblingDependency = parse("|    +--- org.slf4j:slf4j-log4j12:1.7.32");
        DependencyNode result = grandChildNode.add(siblingDependency);

        assertSoftly(
                softly -> {
                    assertThat(result.getRoot().getDependency().getArtifact()).isEqualTo("root");
                    assertThat(result.getChildren().isEmpty()).isTrue();
                    assertThat(result.getLevel()).isEqualTo(2);
                    assertThat(result.getParent().getChildren())
                            .extracting(dependencyNode -> dependencyNode.getDependency().getArtifact())
                            .contains("slf4j-api", "slf4j-log4j12");
                }
        );
    }

    @Test
    void getRoot() {
        Dependency grandChildDependency = parse("|    +--- org.slf4j:slf4j-api:1.7.32");
        DependencyNode grandChildNode = new DependencyNode(Dependency.rootDependency("R-O-O-T"))
                .createChild(parse("+--- com.jurrianhartveldt.tools:dependencies-comparator:1.0.0"))
                .createChild(grandChildDependency);

        assertThat(grandChildNode.getLevel()).isEqualTo(2);
        assertThat(grandChildNode.getRoot().getDependency().getArtifact()).isEqualTo("R-O-O-T");
    }

    @Test
    void sortByGavArtifact() {
        DependencyNode tree = new DependencyNode(Dependency.rootDependency("root"))
                .add(parse("+--- com.jurrianhartveldt.tools:dependencies-xortool:1.0.0"))
                .add(parse("+--- com.jurrianhartveldt.tools:dependencies-comparator:1.1.0"))
                .add(parse("+--- com.jurrianhartveldt.tools:dependencies-adder:2.0.0"))
                .getRoot();

        tree.sortByGAV();

        assertThat(tree.getChildren()).extracting(node -> node.getDependency().getArtifact())
                .containsSequence("dependencies-adder", "dependencies-comparator", "dependencies-xortool");
    }

    @Test
    void sortByGavVersion() {
        DependencyNode tree = new DependencyNode(Dependency.rootDependency("root"))
                .add(parse("+--- com.jurrianhartveldt.tools:dependencies-comparator:2.1.0"))
                .add(parse("+--- com.jurrianhartveldt.tools:dependencies-comparator:1.1.1"))
                .add(parse("+--- com.jurrianhartveldt.tools:dependencies-comparator:3.5.0"))
                .getRoot();

        tree.sortByGAV();

        assertThat(tree.getChildren()).extracting(node -> node.getDependency().getVersion())
                .containsSequence("1.1.1", "2.1.0", "3.5.0");
    }

    @Test
    void sortByGavGroupBeforeArtifactBeforeVersion() {
        DependencyNode tree = new DependencyNode(Dependency.rootDependency("root"))
                .add(parse("+--- com.jurrianhartveldt.archives:dependencies-printer:2.2.2"))
                .add(parse("+--- com.jurrianhartveldt.tools:dependencies-comparator:4.4.4"))
                .add(parse("+--- com.jurrianhartveldt.tools:dependencies-comparator:1.1.1"))
                .add(parse("+--- com.jurrianhartveldt.archives:dependencies-comparator:3.3.3"))
                .getRoot();

        tree.sortByGAV();

        assertThat(tree.getChildren()).extracting(node -> node.getDependency().getGavKey())
                .containsSequence("com.jurrianhartveldt.archives:dependencies-comparator:3.3.3",
                        "com.jurrianhartveldt.archives:dependencies-printer:2.2.2",
                        "com.jurrianhartveldt.tools:dependencies-comparator:1.1.1",
                        "com.jurrianhartveldt.tools:dependencies-comparator:4.4.4");
    }
}
