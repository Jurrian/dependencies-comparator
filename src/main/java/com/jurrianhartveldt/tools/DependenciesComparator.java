package com.jurrianhartveldt.tools;


import com.jurrianhartveldt.tools.model.DependencyFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class DependenciesComparator {

    private static final Logger LOGGER = LoggerFactory.getLogger(DependenciesComparator.class);

    public static void main(String[] args) {

        if (args == null || args.length != 2) {
            LOGGER.error("Please provide two paths to gradle dependencies files");
            System.exit(-1);
        }

        LOGGER.info("""
                Starting program execution with the following files:
                - %s
                - %s
                """.formatted(args[0], args[1]));

        try {
            DependencyFile dependencyFile1 = DependencyFile.read(args[0]);
            DependencyFile dependencyFile2 = DependencyFile.read(args[1]);

            dependencyFile1.sortByGAV();
            dependencyFile2.sortByGAV();
        } catch (IOException e) {
            LOGGER.error("Failed to parse files", e);
        }



        LOGGER.info("Executed program succesfully");
    }
}
