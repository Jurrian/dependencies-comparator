package com.jurrianhartveldt.tools.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.util.stream.Collectors.toUnmodifiableList;

public class DependencyFile {

    private String fileName;

    private DependencyNode tree;

    public DependencyFile(String fileName, DependencyNode tree) {
        this.fileName = fileName;
        this.tree = tree;
    }

    public String getFileName() {
        return fileName;
    }

    public DependencyNode getTree() {
        return tree;
    }

    public void sortByGAV() {
        tree.sortByGAV();
    }

    public static DependencyFile read(String filePath) throws IOException {

        Path path = Paths.get(filePath);

        if (Files.notExists(path)) {
            throw new FileNotFoundException(format("File '%s' does not exist", filePath));
        }

        String dependenciesFileName = path.getFileName().toString();
        List<Dependency> dependencies;

        try (Stream<String> stream = Files.lines(path)) {
            dependencies = stream.filter(Dependency::isDependency)
                    .map(Dependency::parse)
                    .collect(toUnmodifiableList());
        }

        DependencyNode tree = new DependencyNode(Dependency.rootDependency(dependenciesFileName));
        for (Dependency dependency : dependencies) {
            tree = tree.add(dependency);
        }
        tree = tree.getRoot();

        return new DependencyFile(dependenciesFileName, tree);
    }
}
