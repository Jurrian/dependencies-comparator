package com.jurrianhartveldt.tools.model;

public class Dependency {

    private static final int KEY_LENGTH = 5;
    private static final String MAIN_KEY = "+--- ";
    private static final String END_KEY = "\\--- ";

    private static final String GAV_SEPARATOR = ":";
    private static final String FORCE_VERSION_KEY = " -> ";

    private int depth;

    private String group;

    private String artifact;

    private String version;

    private boolean versionOverwritten;

    // TODO: consider supporting/registring
    //    (c) - dependency constrain
    //    (*) - dependencies omitted (listed previously)

    static boolean isDependency(String line) {
        return line.contains(MAIN_KEY) || line.contains(END_KEY);
    }

    static Dependency parse(String line) {
        Builder builder = new Builder();

        int index = line.indexOf(MAIN_KEY);
        if (index == -1) {
            index = line.indexOf(END_KEY);
        }

        // TODO: improve indexOf from index to avoid substringing in between (?)

        index += KEY_LENGTH;
        builder.withDepth(index / KEY_LENGTH);
        line = line.substring(index);

        index = line.indexOf(GAV_SEPARATOR);
        builder.withGroup(line.substring(0, index));
        line = line.substring(index + GAV_SEPARATOR.length());

        String versionSeparationKey;
        if (line.contains(GAV_SEPARATOR)) {
            versionSeparationKey = GAV_SEPARATOR;
        } else {
            versionSeparationKey = FORCE_VERSION_KEY;
            builder.withOverwrittenVersion();
        }

        index = line.indexOf(versionSeparationKey);
        builder.withArtifact(line.substring(0, index));
        line = line.substring(index + versionSeparationKey.length());


        index = line.indexOf(' ');
        builder.withVersion(index == -1 ? line : line.substring(0, index));

        // TODO: constraint or omitted

        return builder.build();
    }

    public int getDepth() {
        return depth;
    }

    public String getGroup() {
        return group;
    }

    public String getArtifact() {
        return artifact;
    }

    public String getVersion() {
        return version;
    }

    public boolean isVersionOverwritten() {
        return versionOverwritten;
    }

    public String getGavKey() {
        return getGroup() + GAV_SEPARATOR + getArtifact() + GAV_SEPARATOR + getVersion();
    }

    public static Dependency rootDependency(String dependencyTitle) {
        return new Builder()
                .withDepth(0)
                .withGroup("")
                .withArtifact(dependencyTitle)
                .withVersion("")
                .build();
    }

    private static class Builder {

        private Dependency dependency;

        Builder() {
            dependency = new Dependency();
            dependency.versionOverwritten = false;
        }

        private Builder withDepth(int depth) {
            dependency.depth = depth;
            return this;
        }

        private Builder withGroup(String group) {
            dependency.group = group;
            return this;
        }

        private Builder withArtifact(String artifact) {
            dependency.artifact = artifact;
            return this;
        }

        private Builder withVersion(String version) {
            dependency.version = version;
            return this;
        }

        private Builder withOverwrittenVersion() {
            dependency.versionOverwritten = true;
            return this;
        }

        private Dependency build() {
            return dependency;
        }

    }
}
