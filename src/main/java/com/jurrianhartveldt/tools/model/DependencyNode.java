package com.jurrianhartveldt.tools.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static java.lang.CharSequence.compare;

public class DependencyNode {

    private static final Comparator<DependencyNode> DEPENDENCY_NODE_GAV_COMPARATOR = (o1, o2) -> compare(o1.getDependency().getGavKey(), o2.getDependency().getGavKey());

    private Dependency dependency;

    private DependencyNode parent = null;

    private List<DependencyNode> children = new ArrayList<>();

    public DependencyNode(Dependency dependency) {
        this.dependency = dependency;
    }

    public Dependency getDependency() {
        return dependency;
    }

    public DependencyNode getParent() {
        return parent;
    }

    public void setParent(DependencyNode parent) {
        this.parent = parent;
    }

    public List<DependencyNode> getChildren() {
        return children;
    }

    public boolean isRoot() {
        return parent == null;
    }

    public int getLevel() {
        if (this.isRoot())
            return 0;
        else
            return parent.getLevel() + 1;
    }

    public void addChild(DependencyNode dependencyNode) {
        dependencyNode.setParent(this);
        children.add(dependencyNode);
    }

    public void sortByGAV() {
        if (children.isEmpty()) {
            return;
        }

        children.sort(DEPENDENCY_NODE_GAV_COMPARATOR);
        children.forEach(DependencyNode::sortByGAV);
    }

    /**
     * Creates a new DependencyNode child from the supplied dependency and sets parent and child relationship
     *
     * @param {@Link Dependency} the dependency that is to be a child of the current DependencyNode
     * @return {@link DependencyNode} the new child node
     */
    public DependencyNode createChild(Dependency dependency) {
        DependencyNode newChild = new DependencyNode(dependency);

        addChild(newChild);

        return newChild;
    }

    /**
     * Add new dependency relative to current node's vertical branch; depending on depth.
     *
     * @param dependency the new dependency
     * @return the newly created {@link DependencyNode}
     */
    public DependencyNode add(Dependency dependency) {
        // NOTE: dependency is added in order, and so can only ever be a direct child or one of the parent's children.
        return dependency.getDepth() > getLevel() ? createChild(dependency) : parent.add(dependency);
    }

    public DependencyNode getRoot() {
        return isRoot() ? this : parent.getRoot();
    }
}
